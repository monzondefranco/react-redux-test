import React from 'react'
import ReactDom from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Redirect, Switch  } from 'react-router-dom'
import Results from './components/results'
import Details from './components/details'

const Root = (
    <Router>
        <Switch>
            <Route path='/results' component={Results} />
            <Route path='/details/:items' component={Details} />
            <Redirect from='/' to='results'  />
        </Switch>
    </Router>
);


ReactDom.render(Root, document.getElementById('root'));